const table = document.createElement('table');

function tableCreate() {
    table.classList.add('table');

    const tBody = document.createElement('tbody');
    tBody.classList.add('tBody');
    table.appendChild(tBody);

        for (let i = 0; i < 30; i++) {
            const tr = document.createElement('tr');
            tBody.appendChild(tr);

            for (let j = 0; j < 30; j++) {
                const td = document.createElement('td');
                td.classList.add('cell');
                tr.appendChild(td);
            }
        }
     document.body.appendChild(table);
}

tableCreate();

document.addEventListener('click', (e) => {
    if (e.target.tagName === 'TD') {
        e.target.classList.toggle('cell-active');
    }
    else {
        table.classList.toggle('table-active')
    }
});